<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Authentication Routes
  |--------------------------------------------------------------------------
 */
Route::prefix('api/v1/auth')->namespace('User\Api\V1')->group(function () {
    Route::post('login', 'AuthenticationController@login')->middleware('api');
    Route::post('logout', 'AuthenticationController@logout')->middleware('api');
    Route::post('register', 'AuthenticationController@register');
    Route::post('refresh', 'AuthenticationController@refresh')->middleware('api');
    Route::get('me', 'AuthenticationController@me')->middleware('api');
    Route::post('activate', 'AuthenticationController@activate');
    Route::get('show/{userId}', 'AuthenticationController@show')->middleware('api');
    Route::post('send-sms', 'AuthenticationController@sendSms');
});

// Route::group(['prefix' => 'api/v1', 'middleware' => 'auth.jwt'], function() {
//     Route::get('user', 'ApiController@getAuthUser');
//     Route::get('/models/{type?}', 'CarBrandModelCotroller@getAllModels');
//     Route::get('/models/view/{id}', 'CarBrandModelCotroller@showModel')->where('id', '[0-9]+');
//     Route::get('/brands', 'CarBrandModelCotroller@getAllBrands');
//     Route::get('/brands/{id}', 'CarBrandModelCotroller@showBrand');
//     Route::get('/model-brands/{code}', 'CarBrandModelCotroller@getModelBrands');
//     Route::get('/regions', 'RegionController@getAllRegions');
//     Route::get('/regions/{id}', 'RegionController@showRegion');
//     Route::get('/region-types', 'RegionController@getAllRegionTypes');
//     Route::get('/region-type/{region}', 'RegionController@regionsByType');
//     Route::get('/regions-tree/{region}/{subRegion?}', 'RegionController@getRegionsTreeByType');
//     Route::get('/basic', 'BasicController@getBasics');
// });


Route::group(['prefix' => 'api/v1'], function() {

    /*
      |--------------------------------------------------------------------------
      | Car Models V1
      |--------------------------------------------------------------------------
     */
    Route::prefix('cars/models')->namespace('Car\Api\V1')->group(function () {
        Route::get('/', 'CarModelController@index');
        Route::get('/{model}', 'CarModelController@show')->name('api.cars.models.show');
    });

    /*
      |--------------------------------------------------------------------------
      | Car Brands V1
      |--------------------------------------------------------------------------
     */
    Route::prefix('cars/brands')->namespace('Car\Api\V1')->group(function () {
        Route::get('/', 'CarBrandController@index');
        Route::get('/{brand}', 'CarBrandController@show')->name('api.cars.brands.show');
    });

    /*
      |--------------------------------------------------------------------------
      | Cities V1
      |--------------------------------------------------------------------------
     */
    Route::prefix('cities')->namespace('City\Api\V1')->group(function () {
        Route::get('/', 'CityController@cities')->name('api.get.cities');
        Route::get('/{city}/province', 'CityController@cityProvince')->name('api.get.cityProvince');
        Route::get('/{city}', 'CityController@cityDetail')->name('api.get.cityDetail');
    });

    /*
      |--------------------------------------------------------------------------
      | Provinces V1
      |--------------------------------------------------------------------------
     */
    Route::prefix('provinces')->namespace('Province\Api\V1')->group(function () {
        Route::get('/', 'ProvinceController@provinces')->name('api.get.provinces');
        Route::get('/{province}/cities', 'ProvinceController@provinceCities')->name('api.get.provinceProvince');
        Route::get('/{province}', 'ProvinceController@provinceDetail')->name('api.get.provinceDetail');
    });

    /*
      |--------------------------------------------------------------------------
      | ALl Provinces + Cities
      |--------------------------------------------------------------------------
     */
    Route::prefix('regions')->namespace('Region\Api\V1')->group(function () {
        Route::get('/provinces-and-cities', 'RegionController@provincesAndCities')->name('api.get.provincesAndCities');
    });

});
