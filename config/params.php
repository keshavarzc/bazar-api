<?php

return [

    /*
    |--------------------------------------------------------------------------
    | commodityg Params
    |--------------------------------------------------------------------------
    */

    'commodityg' => [
        'car' => ['code' => [1111, 1122], 'subid' => [19, 323]],
        'motorcycle' => ['code' => [], 'subid' => [1540, 1537]],
        'truck' => ['code' => [], 'subid' => [1662, 1664]],
    ],

    'vehicleTypes' => [
        'car' => 'خودرو',
        'motorcycle' => 'موتورسیکلت',
        'truck' => 'خودروی سنگین',
    ],

    'colors' => [
        'silverBlue' => 'نقرابی',
        'black' => 'مشکی',
        'white' => 'سفید',
        'green' => 'سبز',
        'blue' => 'آبی',
        'purple' => 'بنفش',
        'red' => 'قرمز',
        'pink' => 'صورتی',
        'orange' => 'نارنجی',
        'yellow' => 'زرد',
        'brown' => 'قهوه ای',
        'gray' => 'خاکستری',
        'silver' => 'نقره ای ',
        'shell-white' => 'سفید صدفی',
        'graphite' => 'نوک مدادی',
        'dark-blue' => 'سرمه ای',
        'beige' => 'بژ',
        'lead' => 'سربی',
        'titanium' => 'تیتانیوم',
        'copper' => 'مسی',
        'jade-green' => 'یشمی',
        'puce' => 'آلبالویی',
        'coal' => 'ذغالی',
        'black-carbon' => 'کربن بلک',
        'dolphin' => 'دلفینی',
        'olive' => 'زیتونی',
        'cream' => 'کرم',
        'crimson' => 'زرشکی',
        'gold' => 'طلایی',
        'jujube' => 'عنابی',
        'mocha' => 'موکا',
        'tan' => 'برنز',
        'lentil' => 'عدسی',
        'onion-skin' => 'پوست پیازی',
        'khaki' => 'خاکی'
    ],

];
