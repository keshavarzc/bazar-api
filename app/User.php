<?php

namespace App;

use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [
        'password',
        'suptel',
        'supname',
        'supfamily',
        'supcode',
        'active',
        'supemail',
        'create_time',
        'sms_count',
        'lastsms_time'
    ];
    protected $hidden = ['password', 'active'];
    public $timestamps = false;

    /**
     * User constructor.
     * @param array $attributes
     * @param JWTAuth $jwtAuth
     */
    public function __construct(array $attributes = [])
    {
        $this->table = config('table_names.user');

        parent::__construct($attributes);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
