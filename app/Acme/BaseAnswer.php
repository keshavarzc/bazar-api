<?php

namespace App\Acme;

use App\Acme\Contracts\ToJson;
use App\Acme\Contracts\ToArray;
use App\Acme\Contracts\ToObject;

class BaseAnswer implements ToArray, ToJson, ToObject
{
    private $fields = [];
    private $messages = [];
    private $success = false;
    private $errors = [];
    private $data;

    public static $instance = null;

    /**
     * sets the given array or the given values to the related
     * properties and <fields> related indexes.
     *
     * BaseAnswer constructor.
     * @param bool $success
     * @param null $data
     * @param string $message
     * @param array $errors
     */
    public function __construct(bool $success = true, $data = null, string $message = '', array $errors = [])
    {
        if (is_array($success)) {
            $this->setMessage($success['messages']);
            $this->setSuccess($success['success']);
            $this->setData($success['data']);
            $this->setErrors($success['errors']);
        }

        $this->setSuccess($success);
        $this->setData($data);
        $this->setMessage($message);
        $this->setErrors($errors);
    }

    public static function getInstance()
    {
        if (static::$instance == null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function setAll($data)
    {
        $this->setMessage($data['messages']);
        $this->setSuccess($data['success']);
        $this->setData($data['data']);
        $this->setErrors($data['errors']);

        return $this;
    }

    /**
     * sets the <message> property and <fields['message']> at the same
     * time to the passed value
     *
     * @param array|string $messages
     * @return BaseAnswer
     */
    public function setMessage($messages = '')
    {
        if (is_array($messages)) {
            foreach ($messages as $message) {
                if ($message !== '') {
                    array_push($this->messages, $message);
                }
            }
        } else {
            if ($messages !== '') {
                array_push($this->messages, $messages);
            }
        }

        $this->fields['messages'] = $this->messages;
        return $this;
    }

    /**
     * sets the <data> property and <fields['data']> at the same
     * time to the passed value
     *
     * @param $data
     * @return BaseAnswer
     */
    public function setData($data = null)
    {
        $this->data = $this->fields['data'] = $data;
        return $this;
    }

    /**
     * sets the <success> property and <fields['success']> at the same
     * time to the passed value
     *
     * @param bool $success
     * @return BaseAnswer
     */
    public function setSuccess(bool $success = true)
    {
        $this->success = $this->fields['success'] = $success;
        return $this;
    }

    /**
     * sets errors property
     * @param array|string $errors
     * @return $this
     */
    public function setErrors($errors = '')
    {
        if (is_array($errors)) {
            foreach ($errors as $error) {
                if ($error !== '') {
                    array_push($this->errors, $error);
                }
            }
        } else {
            if ($errors !== '') {
                array_push($this->errors, $errors);
            }
        }

        $this->fields['errors'] = $this->errors;
        return $this;
    }

    /**
     * return the <message> property of the class
     *
     * @return array
     */
    public function getMessage(): array
    {
        return $this->messages;
    }

    /**
     * return the <data> property of the class
     *
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * return the <success> property of the class
     *
     * @return bool
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * returns errors property
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Returns all the properties of this class as an array.
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Checks if the given format is compatible with baseAnswer or not.
     * @param $data
     * @return bool
     */
    public function checkFormat($data)
    {
        if (!is_object($data)) {
            return false;
        }

        if (!property_exists($data, 'data')) {
            return false;
        }

        if (!property_exists($data, 'message')) {
            return false;
        }

        if (!property_exists($data, 'status')) {
            return false;
        }

        return true;
    }

    /**
     * Checks for the requested property in the class and
     * if there is not such a property in the class, it
     * will returns the property that exists in the
     * <fields> property and if there is no such
     * index in the <fields> property, It will
     * return null with an error message.
     *
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->fields)) {
            return $this->fields[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE
        );

        return null;
    }

    /**
     * returns The whole properties in array
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->createCleanObject()->getFields();
    }

    /**
     * Returns this object in a json format
     * @return false|string
     */
    public function toJson()
    {
        return json_encode($this->createCleanObject()->getFields());
    }

    /**
     * returns The whole properties in object.
     *
     * @return object
     * @example $this->message, $this->success, $this->data
     */
    public function toObject(): object
    {
        return $this->createCleanObject();
    }

    private function createCleanObject()
    {
        $newObject = new self();

        if (count($this->messages)) {
            $newObject->setMessage($this->getMessage());
        }

        if (count($this->errors)) {
            $newObject->setErrors($this->getErrors());
        }

        if (!is_null($this->data)) {
            $newObject->setData($this->getData());
        }

        $newObject->setSuccess($this->getSuccess());

        return $newObject;
    }
}
