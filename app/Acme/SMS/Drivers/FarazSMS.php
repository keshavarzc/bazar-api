<?php

namespace App\Acme\SMS\Drivers;

use Exception;
use App\Acme\BaseAnswer;
use App\Acme\SMS\SMSInterface;
use App\Acme\SMS\DataInitializer;

/**
 * Class SMS
 *
 *  This Component Build for FarazSMS Panel.
 *  The Document of Component Work Flow Is Exist In Gitlab WIKI.
 *
 */
class FarazSMS extends SMSDriver implements SMSInterface
{
    use DataInitializer;

    protected $username;
    protected $password;
    protected $homeNumber;
    protected $url;
    private $urlWithPattern;


    public function initialData()
    {
        $this->findUser();
        $this->findPassword();
        $this->findHomeNumber();
        $this->findUrl();
    }

    protected function buildUrl($phoneNumber, $params, $pattern)
    {
        $this->url = sprintf(
            $this->urlWithPattern,
            $this->username,
            urlencode($this->password),
            $this->homeNumber,
            $phoneNumber,
            urlencode(json_encode($params)),
            $pattern
        );
    }

    public function sendSMS($phoneNumber, array $params, string $pattern): BaseAnswer
    {
        $phoneNumber = $this->validate($phoneNumber);
        $this->buildUrl($phoneNumber, $params, $pattern);

        $baseAnswer = baseAnswer();

        try {

            $response = $this->client->post($this->url);
            $baseAnswer->setSuccess(true);
            $baseAnswer->setData($response);

        } catch (Exception $exception) {
            logger()->error('problem with curl');
            $baseAnswer->setSuccess(false);
            $baseAnswer->setMessage('problem with curl');
        }

        return $baseAnswer;
    }

    /**
     * It maybe be different for each Panel but Faraz SMS required array
     * format for the given data. you can find more information here:
     * in this link (https://farazsms.com/).
     *
     * @param $data
     * @return array
     */
    public function validate($data)
    {
        return is_array($data) ? $this->toJson($data) : $this->toJson([$data]);
    }

    private function toJson($data)
    {
        return json_encode($data);
    }
}
