<?php

namespace App\Acme\SMS\Drivers;

use GuzzleHttp\Client;

abstract class SMSDriver
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->initialData();
        $this->client = new Client();
    }

    public abstract function initialData();
}
