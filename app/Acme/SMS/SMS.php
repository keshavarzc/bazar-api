<?php

namespace App\Acme\SMS;

class SMS
{
    protected $driver;
    protected $namespace;

    public function __construct()
    {
        $this->findDriver();
    }

    private function findDriver()
    {
        $this->driver = config('sms.driver');
    }

    private function driverStrategy(): SMSInterface
    {
        $driver = config('sms.driver');
        $class = config('sms.' . $driver . '.class');
        return new $class;
    }

    /**
     * Sends SMS to the given phone number with validation code which is
     * passed as the $param argument.
     *
     * @param $phoneNumber
     * @param array $params
     * @param string $pattern
     * @return mixed
     */
    public function send($phoneNumber, $params, string $pattern)
    {
        $driver = $this->driverStrategy();
        return $driver->sendSMS($phoneNumber, $params, $pattern);
    }
}
