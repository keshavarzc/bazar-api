<?php

namespace App\Acme\SMS;

trait DataInitializer
{
    protected function findUser()
    {
        $this->username = config('sms.FarazSMS.username');
    }

    protected function findPassword()
    {
        $this->password = config('sms.FarazSMS.password');
    }

    protected function findHomeNumber()
    {
        $this->homeNumber = config('sms.FarazSMS.homeNumber');
    }

    protected function findPatternCode()
    {
        $this->patternCode = config('sms.FarazSMS.pattern');
    }

    protected function findUrl()
    {
        $this->urlWithPattern = config('sms.FarazSMS.urlWithPattern');
    }
}
