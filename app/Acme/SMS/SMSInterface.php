<?php

namespace App\Acme\SMS;

interface SMSInterface
{
    public function sendSMS($phoneNumber, array $params, string $pattern);
}
