<?php

namespace App\JsonApi\Brands;

use Neomerx\JsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider {

    /**
     * @var string
     */
    protected $resourceType = 'brands';

    /**
     * @param $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource) {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource) {
        return [
            'title' => $resource->name,
//            'created-at' => $resource->created_at->toAtomString(),
//            'updated-at' => $resource->updated_at->toAtomString(),
        ];
    }

}
