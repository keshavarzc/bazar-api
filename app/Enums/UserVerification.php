<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class UserVerification extends Enum
{
    const NOT_VERIFIED =   0;
    const VERIFIED =   1;
}
