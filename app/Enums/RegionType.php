<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class RegionType
 * @package App\Enums
 */
final class RegionType extends Enum
{
    const PROVINCE =   1;
    const CITY =   2;
    const LITTLE_CITY = 3;
    const ALLEY = 4;
    const STREET = 5;
}
