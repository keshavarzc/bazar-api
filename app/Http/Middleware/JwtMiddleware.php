<?php

    namespace App\Http\Middleware;

    use App\Acme\BaseAnswer;
    use Closure;
    use JWTAuth;
    use Exception;
    use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

    class JwtMiddleware extends BaseMiddleware
    {
        /**
         * @var BaseAnswer
         */
        private $baseAnswer;

        public function __construct(BaseAnswer $baseAnswer)
        {
            $this->baseAnswer = $baseAnswer;
        }

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            try {
                $user = JWTAuth::parseToken()->authenticate();
            } catch (Exception $e) {

               $this->baseAnswer->setSuccess(false);
                if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                    $this->baseAnswer->setMessage('توکن شما نامعتبراست');
                }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                    $this->baseAnswer->setMessage('توکن شما منفضی شده است');
                }else{
                    $this->baseAnswer->setMessage('ابتدا وارد سیستم شوید');
                }
                return response()->json($this->baseAnswer->toArray());
            }
            return $next($request);
        }
    }
