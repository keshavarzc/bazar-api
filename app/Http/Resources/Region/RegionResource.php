<?php

namespace App\Http\Resources\Region;

use Illuminate\Http\Resources\Json\JsonResource;

class RegionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->name,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'region' => $this->regionType ? $this->regionType->name : '',
            'regionId' => $this->regionType ? $this->regionType->id : '',
            'parent' => $this->regionParent ? $this->regionParent->name : null,
            'parentId' => $this->parent_id,
        ];
    }
}
