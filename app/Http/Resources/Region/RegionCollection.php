<?php

namespace App\Http\Resources\Region;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RegionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($brand) {
            return [
                'id' => $brand->id,
                'name' => $brand->name,
                'links' => [
                    'self' => route('api.cars.brands.show', $brand)
                ]
            ];
        });
    }
}
