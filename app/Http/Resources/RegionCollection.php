<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RegionCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->name,
            'region' => $this->regionType ? $this->regionType->name : '',
            'region_id' => $this->regionType ? $this->regionType->id : '',
            'parent' => $this->regionParent ? $this->regionParent->name : null,
            'parent_id' => $this->parent_id,
        ];
    }
}
