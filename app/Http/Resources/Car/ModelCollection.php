<?php

namespace App\Http\Resources\Car;

use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;
use Morilog\Jalali\CalendarUtils;
use Illuminate\Support\Collection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ModelCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return Collection
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($model) {

            $now = now();

            $date = [];
            if ($model->shamsi) {
                $jalaliNow = CalendarUtils::toJalali($now->year, $now->month, $now->day);
                $year = $jalaliNow[0];
                for ($i = 1380; $i < $year; $i++) {
                    $date[] = $i;
                }
            } else {
                $year = $now->year;
                for ($i = 2000; $i < $year; $i++) {
                    $date[] = $i;
                }
            }

            return [
                'id' => $model->id,
                'name' => $model->name,
                'date' => $date,
                'links' => [
                    'self' => route('api.cars.models.show', $model)
                ]
            ];
        });
    }
}
