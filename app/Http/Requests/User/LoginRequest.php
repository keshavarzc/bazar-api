<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'suptel' => 'required',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'suptel.required' => 'شماره تلفن الرامی است.',
            'password.required' => 'رمز عبور الزامی است.'
        ];
    }
}
