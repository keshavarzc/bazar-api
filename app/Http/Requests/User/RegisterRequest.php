<?php

namespace App\Http\Requests\User;

use App\Acme\BaseAnswer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'suptel' => 'required',
            'password' => 'required',
            'supname' => 'required',
            'supfamily' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'suptel.required' => 'نام کاربری الزامی است.',
            'suptel.unique' => 'نام کاربری تکراری است.',
            'password.required' => 'رمز عبور الزامی است.',
            'supname.required' => 'نام الزامی است.',
            'supfamily.required' => 'نام خانوادگی الزامی است.',
        ];
    }

    /**
     * Rewire this method to avoid redirecting
     *
     * @param \Illuminate\Support\Facades\Validator $validator
     *
     * @return void
     */
    protected function failedValidation(Validator $validator)
     {
        $baseAnswer = new BaseAnswer();
        $baseAnswer->setSuccess(false);

        $errors = convertExceptionToStringInValidation($validator->errors()->messages());
        $baseAnswer->setMessage($errors);

        throw new HttpResponseException(response()->json($baseAnswer->toArray(), 422));
     }
}
