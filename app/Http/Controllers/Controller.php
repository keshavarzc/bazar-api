<?php

namespace App\Http\Controllers;

use App\Acme\BaseAnswer;
use App\Enums\HttpStatusCode;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function response(
        BaseAnswer $result,
        int $successStatusCode = HttpStatusCode::OK,
        int $failStatusCode = HttpStatusCode::OK
    ) {
        if (!$result->getSuccess()) {
            return failResponse(
                $result,
                $failStatusCode ? $failStatusCode : HttpStatusCode::OK
            );
        }

        return successResponse(
            $result,
            $successStatusCode ? $successStatusCode : HttpStatusCode::OK
        );
    }

    protected function responseWithCollection(
        BaseAnswer $result,
        $collection = null,
        int $successStatusCode = HttpStatusCode::OK,
        int $failStatusCode = HttpStatusCode::OK
    ) {
        if (!$result->getSuccess()) {
            return failResponse(
                $result,
                $failStatusCode ? $failStatusCode : HttpStatusCode::OK
            );
        }

        return successResponse(
            $result,
            $successStatusCode ? $successStatusCode : HttpStatusCode::OK,
            $collection
        );
    }
}
