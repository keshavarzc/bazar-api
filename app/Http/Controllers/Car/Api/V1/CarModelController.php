<?php

namespace App\Http\Controllers\Car\Api\V1;

use App\Services\CarModelService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\Car\ModelResource;
use App\Http\Resources\Car\ModelCollection;

class CarModelController extends Controller
{
    /**
     * @var CarModelService
     */
    private $carModelService;

    /**
     * CarModelController constructor.
     * @param CarModelService $carModelService
     */
    public function __construct(CarModelService $carModelService)
    {
        $this->carModelService = $carModelService;
    }

    /**
     * Display list of the car models.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->responseWithCollection(
            $this->carModelService->modelsWithYear(),
            ModelCollection::class
        );
    }

    public function show(int $modelId)
    {
        return $this->responseWithCollection(
            $this->carModelService->find($modelId),
            ModelResource::class
        );
    }
}
