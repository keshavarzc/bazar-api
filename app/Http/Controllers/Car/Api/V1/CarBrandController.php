<?php

namespace App\Http\Controllers\Car\Api\V1;

use App\Acme\BaseAnswer;
use App\Models\CarBrand;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\Car\BrandResource;

class CarBrandController extends Controller
{
    /**
     * @var CarBrand
     */
    private $carBrand;

    /**
     * @var BaseAnswer
     */
    private $baseAnswer;


    /**
     * CarBrandController constructor.
     * @param $carBrand
     * @param $baseAnswer
     */
    public function __construct(CarBrand $carBrand, BaseAnswer $baseAnswer)
    {
        $this->carBrand = $carBrand;
        $this->baseAnswer = $baseAnswer;
    }

    /**
     * Display list of the car brands.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $this->baseAnswer->setSuccess(true);
        $this->baseAnswer->setMessage('list of car brands');
        $this->baseAnswer->setData(BrandResource::collection($this->carBrand->all()));

        return response()->json($this->baseAnswer->toArray());
    }


    /**
     * Display the specified car brand.
     *
     * @param int $brandId
     * @return JsonResponse
     */
    public function show(int $brandId)
    {
        $brand = $this->carBrand->find($brandId);

        if (!$brand) {
            $this->baseAnswer->setSuccess(false);
            $this->baseAnswer->setMessage('خودرویی با برند درخواستی یافت نشد');

        } else {
            $this->baseAnswer->setSuccess(true);
            $this->baseAnswer->setData(new BrandResource($brand));
        }

        return response()->json($this->baseAnswer->toArray());
    }
}
