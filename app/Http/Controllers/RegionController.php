<?php

namespace App\Http\Controllers;

use App\Models\Region;
use App\Models\RegionType;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\RegionCollection;
use App\Http\Resources\RegionTypeCollection;

class RegionController extends ApiController
{

    /**
     * Display a list of the all regions.
     *
     * @return mixed
     */
    public function getAllRegions()
    {
        $regions = Region::all();
        $regions = $regions ?? [];

        return $this->respond([
            'data' => RegionCollection::collection($regions),
        ]);
    }

    /**
     * Display the specified region-model.
     *
     * @param int $id
     * @return mixed
     */
    public function showRegion($id)
    {
        $regionModel = Region::find($id);

        if (!$regionModel) {
            return $this->responseNotFound('ناحیه درخواستی یافت نشد');
        } else {
            return $this->respond([
                'data' => new RegionCollection($regionModel)
            ]);
        }
    }

   /**
     * Display a list of the all region types.
     *
     * @return mixed
     */
    public function getAllRegionTypes()
    {
        $regionTypes = RegionType::all();
        $regionTypes = $regionTypes ?? [];

        return $this->respond([
            'data' => RegionTypeCollection::collection($regionTypes),
        ]);
    }

    /**
     * Display the specified region-type for instance,"province".
     *
     * @param int $region
     * @return mixed
     */
    public function regionsByType($region)
    {
        $subRegions = Region::getRegionsByCode($region);

        if (!$subRegions) {
            return $this->responseNotFound('زیرمجموعه ی ناحیه درخواستی یافت نشد');
        } else {
            return $this->respond([
                'data' => RegionCollection::collection($subRegions)
            ]);
        }
    }


    /**
     * Display the specified region-type for instance,"province".
     *
     * @param int $region
     * @return mixed
     */
    public function getRegionsTreeByType($region, $subRegion = null)
    {
        $subRegions = Region::getSunRegionsTree($region, $subRegion);

        if (!$subRegions) {
            return $this->responseNotFound('اطلاعاتی یافت نشد');
        } else {
            return $subRegions;
        }
    }
}
