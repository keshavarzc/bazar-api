<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\CarModelCollection;
use App\Http\Resources\CarBrandCollection;

class CarBrandModelCotroller
{
    protected $user;

    /**
     * Display a list of the car-models.
     *
     * @param $type
     * @return mixed
     */
    public function getAllModels($type = 'car')
    {
        $carModels = CarModel::vehicleModels($type);
        $carModels = $carModels ? CarModelCollection::collection($carModels) : [];

        return $this->respond([
            'data' => $carModels,
        ]);
    }


    /**
     * Display the specified car-model.
     *
     * @param int $id
     * @return mixed
     */
    public function showModel($id)
    {
        $carModel = CarModel::find($id);

        if (!$carModel) {
            return $this->responseNotFound('خودرویی با مدل درخواستی یافت نشد');
        } else {

            return $this->respond([
                'data' => new CarModelCollection($carModel)
            ]);
        }
    }

    /**
     * Display a list of the car-brands.
     *
     * @return mixed
     */
    public function getAllBrands(Request $request)
    {
        $limit = $request->input('limit') ?: self::DEFAULT_LIMIT;

        $carBrands = CarBrand::paginate($limit);

        return $this->respondWithPagination($carBrands, [
            'data' => CarBrandCollection::collection($carBrands->all()),
        ]);
    }

    /**
     * Display the specified car-brand.
     *
     * @param int $id
     * @return mixed
     */
    public function showBrand($id)
    {
        $carBrand = CarBrand::find($id);

        if (!$carBrand) {
            return $this->responseNotFound('خودرویی با برند درخواستی یافت نشد');
        } else {

            return $this->respond([
                'data' => new CarBrandCollection($carBrand)
            ]);
        }
    }

    /**
     * Display the brand of a model based on code.
     *
     * @param int $code
     * @return mixed
     */
    public function getModelBrands($code)
    {
        $brands = CarBrand::getModelBrands($code);

        if (!$brands) {

            return $this->responseNotFound('برندهای مدل درخواستی یافت نشد');
        } else {

            return $this->respond([
                'data' => CarBrandCollection::collection($brands->all())
            ]);
        }
    }

}
