<?php

namespace App\Http\Controllers\User\Api\V1;

use App\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;

class AuthenticationController extends Controller
{
    /**
     * @var User
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Loges In a User
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        return $this->response($this->userService->login($request));
    }

    /**
     * Registers a user
     * @param RegisterRequest $request
     * @return RegisterRequest|false|string
     */
    public function register(RegisterRequest $request)
    {
        return $this->response($this->userService->store($request));
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        return $this->response($this->userService->logOut());
    }

    /**
     * Activate a registered user via a 4 digit code which has
     * sent by the SMS Driver to the user's jeb telephone.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function activate(Request $request)
    {
        return $this->response($this->userService->activateUser($request));
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function me()
    {
        return $this->response($this->userService->me());
    }

    /**
     * @param int $userId
     * @return JsonResponse
     */
    public function show(int $userId)
    {
        return $this->response($this->userService->find($userId));
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->response($this->userService->refresh());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function sendSms(Request $request)
    {
        return $this->response($this->userService->sendSms($request));
    }
}
