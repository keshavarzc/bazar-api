<?php

namespace App\Http\Controllers;

use Facade\FlareClient\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class BasicController extends ApiController
{
    /**
     * Return some basic information like colors, ...
     *
     * @return mixed
     */
    public function getBasics()
    {
        $colors = Config::get('params.colors');

        return $this->respond([
                       'data' => ['colors' => $colors]
                    ]);
    }
}
