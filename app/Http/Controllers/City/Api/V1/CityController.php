<?php

namespace App\Http\Controllers\City\Api\V1;

use App\Services\RegionService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * @var RegionService
     */
    private $regionService;

    /**
     * CityController constructor.
     * @param RegionService $regionService
     */
    public function __construct(RegionService $regionService)
    {
        $this->regionService = $regionService;
    }

    /**
     * @return JsonResponse
     */
    public function cities()
    {
        return $this->response($this->regionService->cities());
    }

    /**
     * Display list of city of one province
     *
     * @param $cityId
     * @return JsonResponse
     */
    public function cityProvince($cityId)
    {
        return $this->response($this->regionService->cityProvince($cityId));
    }

    /**
     * Display detial of city
     *
     * @param $cityId
     * @return JsonResponse
     */
    public function cityDetail($cityId)
    {
        return $this->response($this->regionService->cityDetail($cityId));
    }
}
