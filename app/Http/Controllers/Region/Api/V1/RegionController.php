<?php

namespace App\Http\Controllers\Region\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Region;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * @var Region
     */
    private $region;

    /**
     * CityController constructor.
     * @param $region
     */
    public function __construct(Region $region)
    {
        $this->region = $region;
    }

    /**
     * Display list of provinces with their cities
     *
     * @return JsonResponse
     */
    public function provincesAndCities()
    {
        return $this->response($this->region->provinceAndCities());
    }


}
