<?php

namespace App\Http\Controllers\Province\Api\V1;

use App\Models\Region;
use App\Services\RegionService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    /**
     * @var Region
     */
    private $region;
    /**
     * @var RegionService
     */
    private $regionService;

    /**
     * CityController constructor.
     * @param Region $region
     * @param RegionService $regionService
     */
    public function __construct(Region $region, RegionService $regionService)
    {
        $this->region = $region;
        $this->regionService = $regionService;
    }

    /**
     * @return JsonResponse
     */
    public function provinces()
    {
        return $this->response($this->regionService->provinces());
    }

    /**
     * @param $provinceId
     * @return JsonResponse
     */
    public function provinceCities($provinceId)
    {
        return $this->response($this->regionService->provinceCities($provinceId));
    }

    /**
     * @param $provinceId
     * @return JsonResponse
     */
    public function provinceDetail($provinceId)
    {
        return $this->response($this->regionService->provinceDetail($provinceId));
    }
}
