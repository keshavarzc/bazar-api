<?php

namespace App\Services;

use App\Acme\BaseAnswer;
use App\Models\Region;
use App\Enums\RegionType as RegionTypeEnum;

class RegionService
{
    /**
     * @var Region
     */
    private $model;

    public function __construct(Region $model)
    {
        $this->model = $model;
    }

    /**
     * Finds all regions with region_type=1 which means only provinces
     * @return BaseAnswer
     */
    public function provinces(): BaseAnswer
    {
        $provinces = $this->model->where('region_type_id', RegionTypeEnum::PROVINCE)->get();

        if (!count($provinces)) {
            return failAnswer(null, 'موردی یافت نشد.', 'موردی یافت نشد.');
        }

        return successAnswer($provinces, 'لیست استانها');
    }

    /**
     * Show the cities of a province
     * @param $provinceId
     * @return BaseAnswer
     */
    public function provinceCities($provinceId): BaseAnswer
    {
        $province = $this->model->find($provinceId);
        if (!$province) {
            return failAnswer(null, 'استان مورد نظر یافت نشد.', 'استان مورد نظر یافت نشد.');
        }

        $cities = $province->children()
            ->where('region_type_id', RegionTypeEnum::CITY)
            ->get();

        if (!count($cities)) {
            return failAnswer(null, 'موردی یافت نشد', 'موردی یافت نشد');
        }

        return successAnswer($cities, 'لیست شهر های استان');
    }

    /**
     * Show the detail of a province
     * @param int $provinceId
     * @return BaseAnswer
     */
    public function provinceDetail($provinceId): BaseAnswer
    {
        $province = $this->model
            ->where('region_type_id', RegionTypeEnum::PROVINCE)
            ->find($provinceId);

        if (!$province) {
            return failAnswer(null, 'موردی یافت نشد.', 'موردی یافت نشد.');
        }

        return successAnswer($province, 'استان مورد نظر یافت شد.');
    }

    /**
     * Finds all regions with region_type=2 which means only cities
     * @return BaseAnswer
     */
    public function cities(): BaseAnswer
    {
        $cities = $this->model
            ->where('region_type_id', RegionTypeEnum::CITY)
            ->get();

        if (!count($cities)) {
            return failAnswer(null, 'موردی یافت نشد', 'موردی یافت نشد');
        }

        return successAnswer($cities, 'تمامی شهرهای سیستم');
    }

    /**
     * Show the province of a city
     * @param $cityId
     * @return BaseAnswer
     */
    public function cityProvince($cityId): BaseAnswer
    {
        $city = $this->model->find($cityId);
        if (!$city) {
            return failAnswer(null, 'شهر مورد نظر یافت نشد', 'شهر مورد نظر یافت نشد');
        }

        $province = $city->parent()
            ->where('region_type_id', RegionTypeEnum::PROVINCE)
            ->first();

        if (!$province) {
            return failAnswer(null, 'موردی یافت نشد', 'موردی یافت نشد');
        }

        return successAnswer($province, 'استان مورد نظر');
    }

    /**
     * Show the detail of a city
     * @param int $cityId
     * @return BaseAnswer
     */
    public function cityDetail($cityId): BaseAnswer
    {
        $city = $this->model
            ->where('region_type_id', RegionTypeEnum::CITY)
            ->find($cityId);

        if (!$city) {
            return failAnswer(null, 'موردی یافت نشد', 'موردی یافت نشد');
        }

        return successAnswer($city, 'شهر مورد نظر یافت شد');
    }

    public function provinceAndCities(): BaseAnswer
    {
        $regions = $this->getAllProvincesWithCities();

        if (count($regions)) {
            failAnswer(null, 'موردی یافت نشد', 'موردی یافت نشد');
        }

        return successAnswer($regions, 'مناطق جغرافیایی');
    }

    /**
     * Get all provinces with cities
     * @return mixed
     */
    public function getAllProvincesWithCities()
    {
        $resultRs = [];

        $provinces = $this->model
            ->where('region_type_id', RegionTypeEnum::PROVINCE)
            ->get();

        foreach($provinces as $province) {

            $resProvince = [];
            $resProvince['province'] = [
                'id' => $province->id,
                'name' => $province->name,
            ];

            $resProvince['cities'] =  $this->getSunRegionsTree($province->id, RegionTypeEnum::LITTLE_CITY);
            $resultRs[] = $resProvince;
        }

        return $resultRs;
    }


    /**
     * Get the sub regions of a special region to subRegion level
     * for example:  "province level" to "city level"
     *
     * @param int $region
     * @param int $subRegion
     * @return array
     */
    public function getSunRegionsTree($region, $subRegion = null)
    {
        $resultRs = [];

        if ($region) {
            $allRegions = $this->createRegionChildren($region, Null, $subRegion);
            $resultRs = $this->createTreeViewRegions($allRegions, null);
        }

        return $resultRs;
    }

    /**
     * Create a array of regions data
     *
     * @param int $parentId
     * @param int $regionId
     * @param int $subRegion
     * @return array
     */
    public function createRegionChildren($parentId, $regionId = null, $subRegion)
    {

        $regionNodes = $outputNodes = [];

        if ($parentId) {
            $regionNodes = Region::where('parent_id' , $parentId)->get();
        } else {
            $regionNodes = Region::whereNull('parent_id')->get();
        }


        if ($regionNodes) {
            foreach($regionNodes as $index => $node) {
                if ($node->region_type_id <= $subRegion) {
                    //Get region-type data
                    $regionTypeName = $node->regionType->name ?? '';
                    $regionTypeId = $node->regionType->id ?? '';

                    $subNode = [];
                    $subNode['id'] = $node->id;
                    $subNode['title'] = $node->name;
                    $subNode['type'] = $regionTypeName;
                    $subNode['type_id'] = $regionTypeId;
                    $subNode['latitude'] = $node->latitude;
                    $subNode['longitude'] = $node->longitude;
                    $subNode['parent_id'] = is_null($node->parent_id) ? 0 : $node->parent_id;
                    $subNode['nodes'] = array_values(self::createRegionChildren($node->id, $regionId, $subRegion)) ?? [];
                    $outputNodes[] = $subNode;

                    if ($node->id == $regionId) {
                        break;
                    }
                }
            }
        }

        return $outputNodes;
    }

    /**
     * Create a tree view of regions
     *
     * @param array $items
     * @param int $currentParent
     * @return array
     */
    public function createTreeViewRegions($items, $currentParent = null)
    {
        foreach ($items as $itemId => $item) {

            if ($currentParent == $item['parent_id']) {

                if ($item['nodes']) {
                    $nextParent = $item['nodes'][0]['parent_id'] ?? -1;
                    self::createTreeViewRegions($item['nodes'], $nextParent);
                }
            }
        }

        return $items;
    }
}
