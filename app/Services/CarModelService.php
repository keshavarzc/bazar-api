<?php

namespace App\Services;

use Exception;
use App\Acme\BaseAnswer;
use App\Models\CarModel;
use Illuminate\Support\Facades\DB;

class CarModelService
{
    /**
     * @var CarModel
     */
    private $model;

    public function __construct(CarModel $model)
    {
        $this->model = $model;
    }

    public function modelsWithYear(): BaseAnswer
    {
        try {
            $models = config('table_names.model');
            $brands = config('table_names.brand');

            $columns = [
                $models . '.id',
                $models . '.name',
                $brands . '.date_show',
                DB::raw('CASE WHEN ' . $brands . '.date_show=10 THEN 1 ELSE 0 END AS shamsi')
            ];

            $data = $this->model
                ->select($columns)
                ->leftJoin($brands, $brands . '.cgid', '=', $models . '.id')
                ->groupBy($models . '.id')
                ->get();

            return successAnswer($data, 'مدل ماشین های موجود در سیستم');
        } catch (Exception $exception) {
            return failAnswer(null, 'ایرادی پیش آمد', 'ایرادی پیش آمد');
        }
    }

    public function find(int $entityId)
    {
        $entity = $this->model->find($entityId);

        if (!$entity) {
            return failAnswer(null, 'مدل خودرو یافت نشد.', 'مدل خودرو یافت نشد.');
        }

        return successAnswer($entity, 'مدل خودرو');
    }
}
