<?php

namespace App\Services;

use App\User;
use Exception;
use App\Acme\SMS\SMS;
use App\Acme\BaseAnswer;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use App\Enums\UserVerification;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserService
{
    /**
     * @var JWTAuth
     */
    private $jwtAuth;
    /**
     * @var SMS
     */
    private $sms;
    /**
     * @var User
     */
    private $user;

    public function __construct(JWTAuth $jwtAuth, SMS $sms, User $user)
    {
        $this->jwtAuth = $jwtAuth;
        $this->sms = $sms;
        $this->user = $user;
    }

    /**
     * Add new user
     *
     * @param Request $request
     * @return BaseAnswer
     */
    public function store(Request $request): BaseAnswer
    {
        try {

            $user = $this->user->where('suptel', $request->input('suptel'))->first();

            if ($user) {
                return failAnswer(
                    $request->all(),
                    'کاربری با این شماره تلفن قبلا ثبت نام کرده است.',
                    'کاربری با این شماره تلفن قبلا ثبت نام کرده است.'
                );
            }

            $data = [
                'suptel' => $request->input('suptel'),
                'password' => Hash::make($request->input('password')),
                'supname' => $request->input('supname'),
                'supfamily' => $request->input('supfamily'),
                'supcode' => rand(10000, 99999),
                'supemail' => 'fake@email.com',
                'create_time' => now()->timestamp,
                'sms_count' => 0,
                'lastsms_time' => 0,
            ];

            $newUser = $this->user->create($data);

            $this->sendRegistrationSms($newUser->suptel, $newUser->supcode);

            return successAnswer($newUser, 'اطلاعات کاربر با موفقیت ثبت گردید.');
        } catch (Exception $exception) {
            return failAnswer(
                null,
                'عضویت کاربر با خطا روبرو شد.',
                'عضویت کاربر با خطا روبرو شد.'
            );
        }
    }

    /**
     * Send sms for registration code
     * @param $phoneNumber
     * @param $code
     * @return BaseAnswer
     */
    public function sendRegistrationSms($phoneNumber, $code): BaseAnswer
    {
        try {
            $result = $this->sms->send($phoneNumber, ['code' => $code], $pattern = 'iue8w0fcmu');
            return successAnswer($result, 'اس ام اس ارسال شد.');
        } catch (Exception $exception) {
            return failAnswer(
                $exception,
                'اس ام اس ارسال نشد.',
                'اس ام اس ارسال نشد.'
            );
        }
    }

    /**
     * Activate a new user
     *
     * @param Request $request
     * @return BaseAnswer
     */
    public function activateUser(Request $request): BaseAnswer
    {
        $user = $this->user->where('suptel', $request->input('phone'))
            ->where('supcode', $request->input('code'))
            ->first();

        if (!$user) {
            return failAnswer(
                null,
                'کدارسالی برای تایید عضویت اشتباه می باشد',
                'کدارسالی برای تایید عضویت اشتباه می باشد'
            );
        }

        $user->active = 1;
        $user->save();
        $token = auth()->login($user);

        return successAnswer($token, 'عضویت کاربر با موفقیت تایید گردید');
    }

    public function refresh(): BaseAnswer
    {
        try {
            if ($user = auth()->refresh()) {
                return successAnswer($user, 'کاربر لاگین شده + توکن جدید.');
            }

            return failAnswer(null, 'کاربری یافت نشد.', 'کاربری یافت نشد.');
        } catch (TokenExpiredException $exception) {
            return failAnswer(null, 'زمان مصرف توکن به پایان رسیده است.', 'زمان مصرف توکن به پایان رسیده است.');
        } catch (Exception $exception) {
            return failAnswer(null, 'توکن مشکل دارد', 'توکن مشکل دارد');
        }
    }

    public function logOut(): BaseAnswer
    {
        try {
            $tokenFetch = $this->jwtAuth->parseToken()->authenticate();
            $token = $tokenFetch ? true : false;
        } catch (JWTException $e) { //general JWT exception
            $token = false;
        }

        if (!$token) {
            return failAnswer(
                null,
                'اطلاعات ارسالی شما اشتباه می باشد',
                'اطلاعات ارسالی شما اشتباه می باشد'
            );
        }

        try {
            $result = $this->jwtAuth->invalidate($this->jwtAuth->getToken());
            return successAnswer($result, 'خروج از سیستم با موفقیت انجام گردید');
        } catch (JWTException $exception) {
            return failAnswer(
                null,
                'خروج از سیستم با خطا روبرو شد',
                'خروج از سیستم با خطا روبرو شد'
            );
        }
    }

    public function login(Request $request): BaseAnswer
    {
        $user = $this->user->where('suptel', $request->input('suptel'))->first();

        if (!$user) {
            return failAnswer(
                $request->all(),
                'اطلاعات وارد شده اشتباه است.',
                'اطلاعات وارد شده اشتباه است.'
            );
        }

        if (!Hash::check($request['password'], $user->getAuthPassword())) {
            return failAnswer(
                $request->all(),
                'اطلاعات وارد شده اشتباه است.',
                'اطلاعات وارد شده اشتباه است.'
            );
        }

        if ($user->active == UserVerification::NOT_VERIFIED) {
            return failAnswer($user, 'کاربر تایید نشده است', 'کاربر تایید نشده است');
        }

        return successAnswer($this->jwtAuth->fromUser($user), 'کاربر با موفقیت وارد سایت شد.');
    }

    public function sendSms(Request $request): BaseAnswer
    {
        $user = $this->user->where('suptel', $request->input('suptel'))->first();

        if (!$user) {
            return failAnswer(
                null,
                'شماره تلفن وارد شده اشتباه است',
                'شماره تلفن وارد شده اشتباه است'
            );
        }

        return $this->sendRegistrationSms($request->input('suptel'), $user->supcode);
    }

    public function find(int $userId): BaseAnswer
    {
        $user = $this->user->find($userId);

        if (!$user) {
            return failAnswer(null, 'کاربری یافت نشد.', 'کاربری یافت نشد.');
        }

        return successAnswer($user, 'کاربر با موفقیت یافت شد.');
    }

    public function me(): BaseAnswer
    {
        $user = auth()->user();
        if (!$user) {
            return failAnswer(null, 'کاربری یافت نشد.', 'کاربری یافت نشد.');
        }

        return successAnswer($user, 'کاربر یافت شد.');
    }
}
