<?php

namespace App\Models;

use App\Models\CarModel;
use Illuminate\Database\Eloquent\Model;

class CarBrand extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'commodity';

    public function __construct(array $attributes = [])
    {
        $this->table = config('table_names.brand');
        parent::__construct($attributes);
    }

    /**
     * Get the car(model) record associated with the brand.
     */
    public function model()
    {
        return $this->hasOne('App\Models\CarModel', 'id', 'cgid');
    }

    /**
     * Get the brands of a model based on code.
     *
     * @param int $code
     * @return mixed
     */
    public static function getModelBrands($code)
    {
        $brands = CarBrand::select(['id', 'name', 'slug', 'cgid'])
            ->where('all_code', 'like', '%' . $code . '%')
            ->orderBy('name')
            ->get();

        return $brands;
    }
}
