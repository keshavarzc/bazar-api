<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public function __construct(array $attributes = [])
    {
        $this->table = config('table_names.region');

        parent::__construct($attributes);
    }

    /**
     * Get the region parent record associated with the region.
     */
    public function parent()
    {
        return $this->hasOne(Region::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Region::class, 'parent_id', 'id');
    }

    /**
     * Get the region type record associated with the region.
     */
    public function regionType()
    {
        return $this->belongsTO(RegionType::class);
    }

    /**
     * Get the sub regions of a special region
     * @param int $region
     * @return mixed
     */
    public function getRegionsByCode($region)
    {
        return $this->where('region_type_id', $region)
            ->orderBy('name')
            ->get();
    }
 }



