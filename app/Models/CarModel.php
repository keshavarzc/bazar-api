<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CarModel extends Model
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'slug', 'subid', 'techspec',
        'cgspec', 'techgroupbranchcode', 'cgrange'
    ];


    public function __construct(array $attributes = [])
    {
        $this->table = config('table_names.model');
        parent::__construct($attributes);
    }



    /**
     * Get the models according to "subid and code".
     *
     * @param string $type
     * @return mixed
     */
    public static function vehicleModels($type)
    {

        $modelsList = [];

        //Get parmas for vehicleTypes
        $vehicleTypes = Config::get('params.vehicleTypes');

        //Get parmas for search conditional in db
        $vehicleConditions = Config::get('params.commodityg');

        if (in_array($type, array_keys($vehicleConditions))) {

            $modelsList = CarModel::select(['id', 'name', 'slug', 'code'])
                ->where(function ($query) use ($type, $vehicleConditions) {
                    //TODO: high level codes! Congrats!!
                    foreach ($vehicleConditions[$type]['subid'] as $select) {
                        $query->orWhere('subid', '=', $select);
                    }

                    foreach ($vehicleConditions[$type]['code'] as $select) {
                        $query->orWhere('code', '%', $select);
                    }
                })->get();

        }

        return $modelsList;
    }
}
