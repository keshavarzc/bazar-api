<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegionType extends Model
{
    public function __construct(array $attributes = [])
    {
        $this->table = config('table_names.region_type');

        parent::__construct($attributes);
    }

    public function region()
    {
        return $this->hasOne(Region::class);
    }
}
