<?php

use App\Enums\HttpStatusCode;
use Carbon\Carbon;
use App\Acme\BaseAnswer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Morilog\Jalali\CalendarUtils;

/**
 * add a specific days to the current time
 *
 * @param $date
 * @return mixed
 * @example 1389/2/7 => 1389/2/14
 *
 */
function convertGregorianToJalali($date)
{
    return CalendarUtils::strftime('Y-m-d', strtotime($date));
}

function addDaysToNowGregorian($days)
{
    return date('Y-m-d', strtotime(now() . $days));
}

/**
 * Converts Jalali date time to Gregorian
 *
 * @param $dateString
 * @return string
 * @example 1398/02/25 => 2019/05/19
 *
 */
function convertJalaliToGregorian($dateString)
{
    return CalendarUtils::convertNumbers($dateString, true);
}

/**
 * converts a time stamp to a optionally formatted Gregorian date
 *
 * @param $timeStamp
 * @param string $format
 * @return string
 */
function convertTimeStampToGregorian($timeStamp, $format = 'Y-m-d')
{
    return Carbon::createFromTimestamp($timeStamp)->format($format);
}

/**
 * @param bool $singleTon
 * @return BaseAnswer
 */
function baseAnswer($singleTon = false)
{
    if ($singleTon) {
        return BaseAnswer::getInstance();
    }

    return new BaseAnswer();
}


/**
 * convert array(exception messages) to string
 *
 * @param $validationErrors
 * @return string
 */
function convertExceptionToStringInValidation($validationErrors)
{
    $errors = '';

    foreach ($validationErrors as $messages) {
        foreach ($messages as $message) {
            $errors .= $message . ', ';
        }
    }

    $errors = $errors ? rtrim($errors, ', ') : '';

    return $errors;
}

/**
 * @param $data
 * @param array|string $message
 * @param array|string $errors = null
 * @return BaseAnswer
 */
function successAnswer($data, $message, $errors = null): BaseAnswer
{
    $baseAnswer = baseAnswer()
        ->setData($data)
        ->setMessage($message)
        ->setSuccess(true);

    if ($errors) {
        $baseAnswer->setErrors($errors);
    }

    return $baseAnswer;
}

/**
 * @param $data
 * @param array|string $message
 * @param array|string $errors
 * @return BaseAnswer
 */
function failAnswer($data, $message, $errors = null): BaseAnswer
{
    $baseAnswer = baseAnswer()
        ->setData($data)
        ->setMessage($message)
        ->setSuccess(false);

    if ($errors) {
        $baseAnswer->setErrors($errors);
    }

    return $baseAnswer;
}

/**
 * @param BaseAnswer $result
 * @param $statusCode
 * @param null $collection
 * @return JsonResponse
 */
function successResponse(BaseAnswer $result, $statusCode, $collection = null): JsonResponse
{
    $baseAnswer = baseAnswer()
        ->setSuccess(true)
        ->setErrors($result->getErrors())
        ->setMessage($result->getMessage());

    if ($collection) {
        $baseAnswer->setData(new $collection($result->getData()));
    } else {
        $baseAnswer->setData($result->getData());
    }

    return response()->json(
        $baseAnswer->toArray(),
        $statusCode
    );
}

/**
 * @param BaseAnswer $baseAnswer
 * @param $statusCode
 * @return JsonResponse
 */
function failResponse(BaseAnswer $baseAnswer, $statusCode): JsonResponse
{
    return response()->json(
        baseAnswer()
            ->setSuccess(false)
            ->setData($baseAnswer->getData())
            ->setMessage($baseAnswer->getMessage())
            ->setErrors($baseAnswer->getErrors())
            ->toArray(),
        $statusCode
    );
}

function responseWithOutCollection(
    BaseAnswer $result,
    int $successStatusCode = HttpStatusCode::OK,
    int $failStatusCode = HttpStatusCode::NOT_FOUND
) {
    if (!$result->getSuccess()) {
        return failResponse(
            $result,
            $failStatusCode ? $failStatusCode : HttpStatusCode::NOT_FOUND
        );
    }

    return successResponse(
        $result,
        $successStatusCode ? $successStatusCode : HttpStatusCode::OK
    );
}

function responseWithCollection(
    BaseAnswer $result,
    $collection = null,
    int $successStatusCode = HttpStatusCode::OK,
    int $failStatusCode = HttpStatusCode::NOT_FOUND
) {
    if (!$result->getSuccess()) {
        return failResponse(
            $result,
            $failStatusCode ? $failStatusCode : HttpStatusCode::NOT_FOUND
        );
    }

    return successResponse(
        $result,
        $successStatusCode ? $successStatusCode : HttpStatusCode::OK,
        $collection
    );
}
