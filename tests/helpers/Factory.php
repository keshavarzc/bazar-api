<?php

namespace Tests\helpers;

use App\Models\CarModel;

trait Factory
{

   /**
    * @var integer
    */
   protected $times = 1;


   /**
    * Number of times to create records
    *
    * @param $count
    * @return $this
    */
   public function times($count)
   {
      $this->times = $count;
      return $this;
   }


   /**
    * Make a new record in the DB
    * @param $type
    * @param array $fields
    */
   public function make($type, array $fields = [])
   {

      while($this->times--)
      {
         $stub = array_merge($this->getStub(), $fields);
         $type::create($stub);
      }
   }

   abstract protected function getStub();
}
