<?php

namespace Tests\helpers;

use BadMethodCallException;
use Faker\Factory as Faker;
use App\Models\CarModel;
use Tests\helpers\Factory;
use Tests\TestCase;

abstract class ApiTester extends TestCase
{
   use Factory;

   /**
    * @var Faker
    */
   protected $fake;

   /**
    * Initialize function
    */
   public function __construct()
   {
      parent::__construct();
      $this->fake = Faker::create();
   }

   /**
    * Get JSON output from API
    *
    * @param $url
    * @return mixed
    */
   public function getJsonUrl($url, $method = 'GET')
   {
       return \json_decode($this->call($method, $url)->getContent());
   }


   /**
    * Call APi urls
    *
    * @param $url
    * @return mixed
    */
   public function callUrl($url)
   {
       return $this->call('GET', $url);
   }

   /**
    * Check the attribute after fetch one record
    *
    * @return mixed
    */
   public function assertObjectHasAttributes()
   {
       $args = func_get_args();
       $object = array_shift($args);

       foreach($args as $attribute)
       {
          $this->assertObjectHasAttribute($attribute, $object);
       }
   }
}
