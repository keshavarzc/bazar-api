<?php

namespace Tests\Feature;

use App\Models\CarModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\helpers\ApiTester;

class BrandModelTest extends ApiTester
{
   /** @test */
   public function just_with_token_can_fetch_all_models()
   {
      $this->assertTrue(true);
   }


   /** @test */
   public function it_fetches_all_models()
   {
      $this->times(1)->make(CarModel::class);
      $this->getJsonUrl('api/v1/models', 'GET');
      $this->assertTrue(true);
   }

   /** @test */
   public function it_fetch_a_single_model()
   {
      $this->make(CarModel::class);

      $model = $this->getJsonUrl('api/v1/models/view/150', 'GET')->data;
      $this->assertObjectHasAttributes($model, 'title', 'slug');
   }

   /** @test */
   public function it_404s_if_a_model_is_not_found()
   {
      $response = $this->callUrl('/api/v1/models/view/x');
      $response->assertStatus(404);
   }

   protected function getStub()
   {
       $name = $this->fake->sentence;

       return [
         'name' => $name,
         'code' => 1122,
         'slug' => $name,
         'subid' => 0,
         'techspec' => '',
         'cgspec' => '',
         'techgroupbranchcode' => '',
         'cgrange' => '',
      ];
   }
}
